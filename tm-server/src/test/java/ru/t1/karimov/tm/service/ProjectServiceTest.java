package ru.t1.karimov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.model.IProjectService;
import ru.t1.karimov.tm.api.service.model.IProjectTaskService;
import ru.t1.karimov.tm.api.service.model.ITaskService;
import ru.t1.karimov.tm.api.service.model.IUserService;
import ru.t1.karimov.tm.configuration.ServerConfiguration;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.NameEmptyException;
import ru.t1.karimov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.karimov.tm.exception.field.TaskIdEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.migration.AbstractSchemeTest;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectServiceTest extends AbstractSchemeTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IProjectService projectService;

    @NotNull
    private static IProjectTaskService projectTaskService;

    @NotNull
    private static ITaskService taskService;

    @NotNull
    private static IUserService userService;

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        propertyService = context.getBean(IPropertyService.class);
        projectService = context.getBean(IProjectService.class);
        projectTaskService = context.getBean(IProjectTaskService.class);
        taskService = context.getBean(ITaskService.class);
        userService = context.getBean(IUserService.class);

        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initTest() throws Exception {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        @NotNull final User testAdmin = new User();
        testAdmin.setLogin("testAdmin");
        testAdmin.setPasswordHash(HashUtil.salt(propertyService, "testAdmin"));
        testAdmin.setEmail("testAdmin@testAdmin.ru");
        testAdmin.setLastName("admin");
        testAdmin.setFirstName("admin");
        testAdmin.setMiddleName("admin");
        testAdmin.setRole(Role.ADMIN);
        @NotNull final User testUser = new User();
        testUser.setLogin("testUser");
        testUser.setPasswordHash(HashUtil.salt(propertyService, "testUser"));
        testUser.setEmail("testUser@testUser.ru");
        testUser.setLastName("user");
        testUser.setFirstName("user");
        testUser.setMiddleName("user");
        userService.add(testAdmin);
        userService.add(testUser);
        projectList.add(new Project(testAdmin, "Project 1", "admin project", Status.IN_PROGRESS));
        projectList.add(new Project(testAdmin, "Project 2", "admin project", Status.IN_PROGRESS));
        projectList.add(new Project(testUser, "Project 1", "user project", Status.NOT_STARTED));
        projectList.add(new Project(testUser, "Project 2", "user project", Status.IN_PROGRESS));
        taskList.add(new Task(testAdmin, "Task 1", "Project 1", Status.IN_PROGRESS));
        taskList.add(new Task(testAdmin, "Task 2", "Project 2", Status.IN_PROGRESS));
        taskList.add(new Task(testUser, "Task 1", "Project 1", Status.IN_PROGRESS));
        taskList.add(new Task(testUser, "Task 2", "Project 2", Status.NOT_STARTED));
        for (@NotNull final Project project : projectList) {
            projectService.add(project);
        }
        for (@NotNull final Task task : taskList) {
            @NotNull final String projectName = task.getDescription();
            @NotNull final User user = task.getUser();
            @NotNull final Project joinProject = projectList.stream()
                    .filter(m -> projectName.equals(m.getName()))
                    .filter(m -> user.equals(m.getUser()))
                    .findFirst()
                    .orElse(new Project());
            taskService.add(task);
            projectTaskService.bindTaskToProject(user.getId(), joinProject.getId(), task.getId());
        }
    }

    @After
    public void clean() throws Exception {
        userService.removeOneByLogin("testUser");
        userService.removeOneByLogin("testAdmin");
    }

    @Test
    public void testAdd() throws Exception {
        final long expectedSize = projectService.getSize() + 1;
        @Nullable final User user = userService.findByLogin("testUser");
        assertNotNull(user);
        @NotNull final Project project = new Project();
        project.setName("Test Project");
        project.setDescription("Test");
        project.setUser(user);
        projectService.add(user.getId(), project);
        assertEquals(expectedSize, projectService.getSize().intValue());
    }

    @Test
    public void testAddAll() throws Exception {
        final long expectedSize = projectService.getSize() + 10;
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final User user = Objects.requireNonNull(userService.findByLogin("testUser"));
        for (int i = 1; i <= 10; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Desc " + i);
            project.setUser(user);
            projects.add(project);
        }
        projectService.add(projects);
        assertEquals(expectedSize, projectService.getSize().intValue());
    }

    @Test
    public void testAddTaskToProject() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final String projectId = projectList.get(1).getId();
        assertNotNull(projectTaskService.bindTaskToProject(testUserId, projectId, taskId));
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testAddTaskToProjectEmptyProjectId() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        @NotNull final String projectId = "";
        projectTaskService.bindTaskToProject(testUserId, taskId, projectId);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testAddTaskToProjectEmptyTaskId() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String taskId = "";
        @NotNull final String projectId = projectList.get(1).getId();
        projectTaskService.bindTaskToProject(testUserId, taskId, projectId);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testAddTaskToProjectNegative() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testUser").getId();
        @NotNull final String taskId = taskList.get(2).getId();
        @NotNull final String projectId = projectList.get(0).getId();
        assertNotNull(projectTaskService.bindTaskToProject(testUserId, taskId, projectId));
    }

    @Test
    public void testClearUser() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testUser").getId();
        assertNotEquals(0, projectService.getSize(testUserId).intValue());
        projectService.removeAll(testUserId);
        assertEquals(0, projectService.getSize(testUserId).intValue());
    }

    @Test
    public void testFindAllAdmin() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        final int expectedSize = projectService.getSize(testUserId).intValue();
        @NotNull final List<Project> projectList = projectService.findAll(testUserId);
        assertEquals(expectedSize, projectList.size());
    }

    @Test
    public void testFindAllUser() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testUser").getId();
        @NotNull List<Project> projectList = projectService.findAll(testUserId);
        assertEquals(2, projectList.size());
    }

    @Test
    public void testFindByIdAdmin() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        assertEquals(projectList.get(0).getId(), projectService.findOneById(testUserId, projectId).getId());
    }

    @Test
    public void testFindByIdUser() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testUser").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        assertNull(projectService.findOneById(testUserId, projectId));
    }

    @Test
    public void testGetSizeAdmin() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        assertEquals(2, projectService.getSize(testUserId).intValue());
    }

    @Test
    public void testGetSizeUser() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testUser").getId();
        assertEquals(2, projectService.getSize(testUserId).intValue());
    }

    @Test
    public void testRemoveByIdUser() throws Exception {
        final int expectedSize = projectService.getSize().intValue();
        @NotNull final String testUserId = userService.findByLogin("testUser").getId();
        @NotNull final List<Project> projects = projectService.findAll(testUserId);
        for (@NotNull final Project project : projects) projectService.removeOneById(testUserId, project.getId());
        assertNotEquals(expectedSize, projectService.getSize().intValue());
    }

    @Test
    public void testRemoveProjectById() throws Exception {
        @NotNull final String testAdminId = userService.findByLogin("testAdmin").getId();
        final int expectedProjectSize = projectService.getSize(testAdminId).intValue() - 1;
        final int expectedTaskSize = taskService.getSize(testAdminId).intValue() - 1;
        @NotNull final List<Project> projects = projectService.findAll(testAdminId);
        @NotNull final Project project = projects.stream()
                .filter(m -> "Project 1".equals(m.getName()))
                .findFirst()
                .orElse(new Project());
        @NotNull final String projectId = project.getId();
        projectTaskService.removeProjectById(testAdminId, projectId);
        assertEquals(expectedProjectSize, projectService.getSize(testAdminId).intValue());
        assertEquals(expectedTaskSize, taskService.getSize(testAdminId).intValue());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveProjectByIdBadId() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String projectId = "-----";
        projectTaskService.removeProjectById(testUserId, projectId);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveProjectByIdEmptyId() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String projectId = "";
        projectTaskService.removeProjectById(testUserId, projectId);
    }

    @Test
    public void testRemovedByIdAdmin() throws Exception {
        final int expectedSize = projectService.getSize().intValue();
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectService.removeOneById(testUserId, projectId);
        assertNotEquals(expectedSize, projectService.getSize().intValue());
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        assertNotNull(projectService.updateProjectById(testUserId, projectId, "New name", "New description"));
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdEmptyId() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String projectId = "";
        projectService.updateProjectById(testUserId, projectId, "new name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdEmptyName() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String projectId = projectList.get(0).getId();
        projectService.updateProjectById(testUserId, projectId, "", "new description");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateByIdUnknownId() throws Exception {
        @NotNull final String testUserId = userService.findByLogin("testAdmin").getId();
        @NotNull final String projectId = "------";
        projectService.updateProjectById(testUserId, projectId, "new name", "new description");
    }

}
