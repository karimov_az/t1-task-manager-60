package ru.t1.karimov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.model.ISessionService;
import ru.t1.karimov.tm.api.service.model.IUserService;
import ru.t1.karimov.tm.configuration.ServerConfiguration;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.migration.AbstractSchemeTest;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static ISessionService sessionService;

    @NotNull
    private static IUserService userService;

    @NotNull
    private List<Session> sessionList;

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        propertyService = context.getBean(IPropertyService.class);
        sessionService = context.getBean(ISessionService.class);
        userService = context.getBean(IUserService.class);

        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initTest() throws Exception {
        sessionList = new ArrayList<>();

        @NotNull final User testAdmin = new User();
        testAdmin.setLogin("testAdmin");
        testAdmin.setPasswordHash(HashUtil.salt(propertyService, "testAdmin"));
        testAdmin.setEmail("testAdmin@testAdmin.ru");
        testAdmin.setLastName("admin");
        testAdmin.setFirstName("admin");
        testAdmin.setMiddleName("admin");
        testAdmin.setRole(Role.ADMIN);
        @NotNull final User testUser = new User();
        testUser.setLogin("testUser");
        testUser.setPasswordHash(HashUtil.salt(propertyService, "testUser"));
        testUser.setEmail("testUser@testUser.ru");
        testUser.setLastName("user");
        testUser.setFirstName("user");
        testUser.setMiddleName("user");
        @NotNull final User testUser1 = new User();
        testUser1.setLogin("testUser1");
        testUser1.setPasswordHash(HashUtil.salt(propertyService, "testUser1"));
        testUser1.setEmail("testUser1@testAdmin.ru");
        testUser1.setLastName("testUser1");
        testUser1.setFirstName("testUser1");
        testUser1.setMiddleName("testUser1");
        @NotNull final User testUser2 = new User();
        testUser2.setLogin("testUser2");
        testUser2.setPasswordHash(HashUtil.salt(propertyService, "testUser2"));
        testUser2.setEmail("testUser2@testUser.ru");
        testUser2.setLastName("testUser2");
        testUser2.setFirstName("testUser2");
        testUser2.setMiddleName("testUser2");
        userService.add(testAdmin);
        userService.add(testUser);
        userService.add(testUser1);
        userService.add(testUser2);
        @NotNull final Session testAdminSession = new Session();
        testAdminSession.setUser(testAdmin);
        @NotNull final Session testUserSession = new Session();
        testUserSession.setUser(testUser);
        sessionService.add(testAdminSession);
        sessionList.add(testAdminSession);
        sessionService.add(testUserSession);
        sessionList.add(testUserSession);
    }

    @After
    public void clean() throws Exception {
        userService.removeOneByLogin("testUser");
        userService.removeOneByLogin("testAdmin");
        userService.removeOneByLogin("testUser1");
        userService.removeOneByLogin("testUser2");
    }

    @Test
    public void testAdd() throws Exception {
        assertEquals(2, sessionService.getSize().intValue());
        @Nullable final User user = userService.findByLogin("testUser");
        assertNotNull(user);
        @NotNull final Session session = new Session(user, Role.USUAL);
        sessionService.add(session);
        assertEquals(3, sessionService.getSize().intValue());
    }

    @Test
    public void testAddAll() throws Exception {
        @NotNull final List<Session> sessionList = new ArrayList<>();
        @Nullable final User user = userService.findByLogin("testUser");
        assertNotNull(user);
        for (int i = 0; i < 10; i++) {
            sessionList.add(new Session(user, Role.USUAL));
        }
        sessionService.add(sessionList);
        assertEquals(12, sessionService.getSize().intValue());
    }

    @Test
    public void testClear() throws Exception {
        sessionService.removeAll();
        assertEquals(0, sessionService.getSize().intValue());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Session> sessionList = sessionService.findAll();
        assertEquals(2, sessionList.size());
    }

    @Test
    public void testFindById() throws Exception {
        for (@NotNull final Session session : sessionList) {
            assertNotNull(sessionService.findOneById(session.getId()));
        }
    }

    @Test
    public void testGetSize() throws Exception {
        assertEquals(2, sessionService.getSize().intValue());
        @Nullable final User user = userService.findByLogin("testUser");
        assertNotNull(user);
        sessionService.add(new Session(user, Role.USUAL));
        assertEquals(3, sessionService.getSize().intValue());
        sessionService.removeAll();
        assertEquals(0, sessionService.getSize().intValue());
    }

    @Test
    public void testRemoveById() throws Exception {
        assertEquals(2, sessionService.getSize().intValue());
        for (@NotNull final Session session : sessionList) {
            sessionService.removeOneById(session.getId());
        }
        assertEquals(0, sessionService.getSize().intValue());
    }

}
