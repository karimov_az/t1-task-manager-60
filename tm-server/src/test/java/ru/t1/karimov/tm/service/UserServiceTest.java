package ru.t1.karimov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.model.IProjectService;
import ru.t1.karimov.tm.api.service.model.ISessionService;
import ru.t1.karimov.tm.api.service.model.ITaskService;
import ru.t1.karimov.tm.api.service.model.IUserService;
import ru.t1.karimov.tm.configuration.ServerConfiguration;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.entity.EntityNotFoundException;
import ru.t1.karimov.tm.exception.entity.UserNotFoundException;
import ru.t1.karimov.tm.exception.field.EmailEmptyException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.LoginEmptyException;
import ru.t1.karimov.tm.exception.field.PasswordEmptyException;
import ru.t1.karimov.tm.exception.user.ExistsEmailException;
import ru.t1.karimov.tm.exception.user.ExistsLoginException;
import ru.t1.karimov.tm.exception.user.RoleEmptyException;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.migration.AbstractSchemeTest;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Session;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserServiceTest extends AbstractSchemeTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IUserService userService;

    @NotNull
    private static IProjectService projectService;

    @NotNull
    private static ITaskService taskService;

    @NotNull
    private static ISessionService sessionService;

    @NotNull
    private List<User> userList;

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        propertyService = context.getBean(IPropertyService.class);
        projectService = context.getBean(IProjectService.class);
        taskService = context.getBean(ITaskService.class);
        userService = context.getBean(IUserService.class);
        sessionService = context.getBean(ISessionService.class);

        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initTest() throws Exception {
        @NotNull final User testAdmin = new User();
        testAdmin.setLogin("testAdmin");
        testAdmin.setPasswordHash(HashUtil.salt(propertyService, "testAdmin"));
        testAdmin.setEmail("testAdmin@testAdmin.ru");
        testAdmin.setLastName("admin");
        testAdmin.setFirstName("admin");
        testAdmin.setMiddleName("admin");
        testAdmin.setRole(Role.ADMIN);

        @NotNull final User testUser = new User();
        testUser.setLogin("testUser");
        testUser.setPasswordHash(HashUtil.salt(propertyService, "testUser"));
        testUser.setEmail("testUser@testUser.ru");
        testUser.setLastName("user");
        testUser.setFirstName("user");
        testUser.setMiddleName("user");

        @NotNull final User testUser1 = new User();
        testUser1.setLogin("testUser1");
        testUser1.setPasswordHash(HashUtil.salt(propertyService, "testUser1"));
        testUser1.setEmail("testUser1@testAdmin.ru");
        testUser1.setLastName("testUser1");
        testUser1.setFirstName("testUser1");
        testUser1.setMiddleName("testUser1");

        @NotNull final User testUser2 = new User();
        testUser2.setLogin("testUser2");
        testUser2.setPasswordHash(HashUtil.salt(propertyService, "testUser2"));
        testUser2.setEmail("testUser2@testUser.ru");
        testUser2.setLastName("testUser2");
        testUser2.setFirstName("testUser2");
        testUser2.setMiddleName("testUser2");
        testUser2.setLocked(true);

        userList = new ArrayList<>();
        userService.add(testAdmin);
        userService.add(testUser);
        userService.add(testUser1);
        userService.add(testUser2);
        userList.add(testAdmin);
        userList.add(testUser);
        userList.add(testUser1);
        userList.add(testUser2);
    }

    @After
    public void clean() throws Exception {
        if (userService.isLoginExist("testUser")) userService.removeOneByLogin("testUser");
        if (userService.isLoginExist("testAdmin")) userService.removeOneByLogin("testAdmin");
        if (userService.isLoginExist("testUser1")) userService.removeOneByLogin("testUser1");
        if (userService.isLoginExist("testUser2")) userService.removeOneByLogin("testUser2");
    }

    @Test
    public void testAdd() throws Exception {
        assertEquals(4, userService.getSize().intValue());
        @NotNull final User testUser = new User();
        testUser.setLogin("testUser3");
        testUser.setPasswordHash(HashUtil.salt(propertyService, "testUser3"));
        testUser.setEmail("testUser3@testUser.ru");
        testUser.setLastName("user3");
        testUser.setFirstName("user3");
        testUser.setMiddleName("user3");
        userService.add(testUser);
        assertEquals(5, userService.getSize().intValue());
        userService.removeOneByLogin("testUser3");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testAddNegative() throws Exception {
        @Nullable final User user = null;
        userService.add(user);
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final String login = "user8";
        @NotNull final String password = "user8";
        @NotNull final String email = "user8@tst.ru";
        @NotNull final Role role = Role.USUAL;
        @NotNull final String loginUnix = "unix";
        @NotNull final String passwordUnix = "unix";
        @NotNull final String loginLinux = "linux";
        @NotNull final String passwordLinux = "linux";

        assertNotNull(userService.create(login, password));
        assertNotNull(userService.create(loginUnix, passwordUnix, email));
        assertNotNull(userService.create(loginLinux, passwordLinux, role));
        assertEquals(userList.size() + 3, userService.getSize().intValue());

        userService.removeOneByLogin("user8");
        userService.removeOneByLogin("unix");
        userService.removeOneByLogin("linux");
    }

    @Test
    public void testCreateException() {
        @NotNull final Role role = Role.USUAL;
        @NotNull final String loginTest = "testUser2";
        @NotNull final String passwordTest = "testUser2";
        @NotNull final String emailTest = "testUser2@testUser.ru";
        @Nullable final Role roleNull = null;
        @NotNull final String loginNew = "testNew";
        @NotNull final String emailNew = "emailNew@tst.ru";

        assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, passwordTest)
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", passwordTest)
        );
        assertThrows(
                ExistsLoginException.class,
                () -> userService.create(loginTest, passwordTest)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(loginNew, null)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(loginNew, "")
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, passwordTest, emailTest)
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", passwordTest, emailTest)
        );
        assertThrows(
                ExistsLoginException.class,
                () -> userService.create(loginTest, passwordTest, emailTest)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(loginNew, null, emailNew)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(loginNew, "", emailNew)
        );
        assertThrows(
                ExistsEmailException.class,
                () -> userService.create(loginNew, loginNew, emailTest)
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create(null, passwordTest, role)
        );
        assertThrows(
                LoginEmptyException.class,
                () -> userService.create("", passwordTest, role)
        );
        assertThrows(
                ExistsLoginException.class,
                () -> userService.create(loginTest, passwordTest, role)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(loginNew, null, role)
        );
        assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(loginNew, "", role)
        );
        assertThrows(
                RoleEmptyException.class,
                () -> userService.create(loginNew, loginNew, roleNull)
        );
    }

    @Test
    public void testClear() throws Exception {
        userService.removeAll();
        assertEquals(0, userService.getSize().intValue());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<User> users = userService.findAll();
        assertEquals(userList.size(), users.size());
    }

    @Test
    public void testFindById() throws Exception {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @Nullable final User actualUser = userService.findOneById(id);
            assertNotNull(actualUser);
            assertEquals(user.getLogin(), actualUser.getLogin());
        }
        assertThrows(
                IdEmptyException.class, () -> userService.findOneById(null)
        );
        assertThrows(
                IdEmptyException.class, () -> userService.findOneById("")
        );
        assertNull(userService.findOneById("otherId"));
    }

    @Test
    public void testFindByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User actualUser =userService.findByLogin(login);
            assertNotNull(actualUser);
            assertEquals(user.getLogin(), actualUser.getLogin());
        }
        assertThrows(
                LoginEmptyException.class, () -> userService.findByLogin(null)
        );
        assertThrows(
                LoginEmptyException.class, () -> userService.findByLogin("")
        );
        assertNull(userService.findByLogin("otherLogin"));
    }

    @Test
    public void testFindByEmail() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            @Nullable final User actualUser =userService.findByEmail(email);
            assertNotNull(actualUser);
            assertEquals(user.getLogin(), actualUser.getLogin());
        }
        assertThrows(
                EmailEmptyException.class, () -> userService.findByEmail(null)
        );
        assertThrows(
                EmailEmptyException.class, () -> userService.findByEmail("")
        );
        assertNull(userService.findByEmail("otherEmail"));
    }

    @Test
    public void testGetSize() throws Exception {
        assertEquals(userList.size(), userService.getSize().intValue());
    }

    @Test
    public void testIsEmailExist() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            assertTrue(userService.isEmailExist(email));
        }
        assertFalse(userService.isEmailExist("otherEmail"));
        assertFalse(userService.isEmailExist(""));
        assertFalse(userService.isEmailExist(null));
    }

    @Test
    public void testIsLoginExist() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            assertTrue(userService.isLoginExist(login));
        }
        assertFalse(userService.isLoginExist("otherLogin"));
        assertFalse(userService.isLoginExist(""));
        assertFalse(userService.isLoginExist(null));
    }

    @Test
    public void testLockUserByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            userService.lockUserByLogin(login);
            @Nullable final User actualUser = userService.findByLogin(login);
            assertNotNull(actualUser);
            assertTrue(actualUser.getLocked());
        }
        assertThrows(
                LoginEmptyException.class, () -> userService.lockUserByLogin(null)
        );
        assertThrows(
                LoginEmptyException.class, () -> userService.lockUserByLogin("")
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.lockUserByLogin("otherLogin")
        );
    }

    @Test
    public void testRemoveOne() throws Exception {
        @Nullable final User userAdmin = userService.findByLogin("testAdmin");
        assertNotNull(userAdmin);
        @NotNull final String adminId = userAdmin.getId();
        @NotNull final Status status = Status.NOT_STARTED;
        @NotNull final Project projectAdmin = projectService.add(new Project(userAdmin, "P1", "PD1", status));
        taskService.add(new Task(userAdmin, "T1", "TD1", status, projectAdmin));
        @NotNull final Session sessionAdmin = new Session(userAdmin, Role.ADMIN);
        sessionService.add(adminId, sessionAdmin);
        assertTrue(projectService.getSize(adminId) > 0);
        assertTrue(taskService.getSize(adminId) > 0);
        assertTrue(sessionService.getSize(adminId) > 0);

        for (@NotNull final User user : userList) userService.removeOne(user);
        assertEquals(0, userService.getSize().intValue());
        assertEquals(0, projectService.getSize().intValue());
        assertEquals(0, taskService.getSize().intValue());
        assertEquals(0, sessionService.getSize().intValue());

        assertThrows(
                UserNotFoundException.class, () -> userService.removeOne(null)
        );
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            userService.removeOneByLogin(login);
        }
        assertEquals(0, userService.getSize().intValue());

        assertThrows(
                LoginEmptyException.class, () -> userService.removeOneByLogin(null)
        );
        assertThrows(
                LoginEmptyException.class, () -> userService.removeOneByLogin("")
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.removeOneByLogin("otherLogin")
        );
    }

    @Test
    public void testRemoveByEmail() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            userService.removeOneByEmail(email);
        }
        assertEquals(0, userService.getSize().intValue());

        assertThrows(
                EmailEmptyException.class, () -> userService.removeOneByEmail(null)
        );
        assertThrows(
                EmailEmptyException.class, () -> userService.removeOneByEmail("")
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.removeOneByEmail("otherEmail")
        );
    }

    @Test
    public void testSetPassword() throws Exception {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @Nullable final String password = user.getPasswordHash();
            @NotNull final User actualUser = userService.setPassword(id, "newPassword");
            assertNotEquals(password, actualUser.getPasswordHash());
            assertEquals(HashUtil.salt(propertyService, "newPassword"), actualUser.getPasswordHash());

            assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, "")
            );
            assertThrows(
                    PasswordEmptyException.class, () -> userService.setPassword(id, null)
            );
        }

        assertThrows(
                IdEmptyException.class, () -> userService.setPassword(null, "newPassword")
        );
        assertThrows(
                IdEmptyException.class, () -> userService.setPassword("", "newPassword")
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.setPassword("otherId", "newPassword")
        );
    }

    @Test
    public void testUpdateUser() throws Exception {
        @NotNull final String firstName = "New First Name";
        @NotNull final String lastName = "New Last Name";
        @NotNull final String middleName = "New Middle Name";
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            @NotNull final User actualUser = userService.updateUser(id,firstName, lastName, middleName);
            assertEquals(firstName, actualUser.getFirstName());
            assertEquals(lastName, actualUser.getLastName());
            assertEquals(middleName, actualUser.getMiddleName());
        }
        assertEquals(userList.size(), userService.getSize().intValue());

        assertThrows(
                IdEmptyException.class, () -> userService.updateUser("",firstName, lastName, middleName)
        );
        assertThrows(
                IdEmptyException.class, () -> userService.updateUser(null,firstName, lastName, middleName)
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.updateUser("otherId",firstName, lastName, middleName)
        );
    }

    @Test
    public void testUnlockUserByLogin() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            userService.unlockUserByLogin(login);
            @Nullable final User actualUser = userService.findByLogin(login);
            assertNotNull(actualUser);
            assertFalse(actualUser.getLocked());
        }
        assertThrows(
                LoginEmptyException.class, () -> userService.unlockUserByLogin(null)
        );
        assertThrows(
                LoginEmptyException.class, () -> userService.unlockUserByLogin("")
        );
        assertThrows(
                UserNotFoundException.class, () -> userService.unlockUserByLogin("otherLogin")
        );
    }

}
