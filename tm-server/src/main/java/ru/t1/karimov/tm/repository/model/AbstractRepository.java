package ru.t1.karimov.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.api.repository.model.IRepository;
import ru.t1.karimov.tm.comparator.CreatedComparator;
import ru.t1.karimov.tm.comparator.StatusComparator;
import ru.t1.karimov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    private static final String COLUMN_CREATED = "created";

    @NotNull
    private static final String COLUMN_STATUS = "status";

    @NotNull
    private static final String COLUMN_NAME = "name";

    @NotNull
    protected static final String ID = "id";

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected String getSortType(@NotNull final Comparator<?> comparator) {
        if (comparator == CreatedComparator.INSTANCE) return COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return COLUMN_STATUS;
        else return COLUMN_NAME;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final List<M> records = findAll();
        for (@NotNull final M model : records) {
            entityManager.remove(model);
        }
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final M model) {
        if (entityManager.contains(model)) {
            entityManager.remove(model);
        } else {
            @NotNull final AbstractModel reference = entityManager.getReference(model.getClass(), model.getId());
            entityManager.remove(reference);
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) throws Exception {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        removeAll();
        return add(models);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
