package ru.t1.karimov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.dto.IDtoRepository;
import ru.t1.karimov.tm.api.service.dto.IDtoService;
import ru.t1.karimov.tm.dto.model.AbstractDtoModel;
import ru.t1.karimov.tm.exception.entity.EntityNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.IndexIncorrectException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<M extends AbstractDtoModel> implements IDtoService<M> {

    @NotNull
    protected static final String ERROR_INDEX_OUT_OF_BOUNDS = "Error! Index оut of bounds...";

    @NotNull
    @Autowired
    protected IDtoRepository<M> repository;

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final M result = repository.add(model);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@NotNull final Collection<M> models) throws Exception {
        @NotNull final List<M> newModels = new ArrayList<>(repository.add(models));
        return newModels;
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        if (comparator == null) return findAll();
        else return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        return repository.findOneByIndex(index);
    }

    @NotNull
    @Override
    public Long getSize() throws Exception {
        return repository.getSize();
    }

    @Override
    @Transactional
    public void removeAll() throws Exception {
        repository.removeAll();
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        repository.removeOne(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.removeOneById(id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        repository.removeOneByIndex(index);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        @NotNull final List<M> newModels = new ArrayList<>(repository.set(models));
        return newModels;
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        repository.update(model);
    }

}
