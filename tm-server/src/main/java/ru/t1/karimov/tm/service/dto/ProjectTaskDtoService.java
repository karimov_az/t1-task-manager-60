package ru.t1.karimov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.karimov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.karimov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.karimov.tm.exception.field.TaskIdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public TaskDto bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final TaskDto task = Optional
                .ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        taskRepository.update(task);
        return task;
    }

    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final List<TaskDto> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (@NotNull final TaskDto task : tasks) taskRepository.removeOneById(userId, task.getId());
        projectRepository.removeOneById(userId, projectId);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final TaskDto task = Optional
                .ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        taskRepository.update(task);
    }

}
