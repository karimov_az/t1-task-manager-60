package ru.t1.karimov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.dto.IProjectDtoService;
import ru.t1.karimov.tm.api.service.dto.ISessionDtoService;
import ru.t1.karimov.tm.api.service.dto.ITaskDtoService;
import ru.t1.karimov.tm.api.service.dto.IUserDtoService;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.entity.UserNotFoundException;
import ru.t1.karimov.tm.exception.field.EmailEmptyException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.LoginEmptyException;
import ru.t1.karimov.tm.exception.field.PasswordEmptyException;
import ru.t1.karimov.tm.exception.user.ExistsEmailException;
import ru.t1.karimov.tm.exception.user.ExistsLoginException;
import ru.t1.karimov.tm.exception.user.RoleEmptyException;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.Optional;

@Service
@AllArgsConstructor
public final class UserDtoService extends AbstractDtoService<UserDto> implements IUserDtoService {

    @NotNull
    @Autowired
    private final IProjectDtoService projectService;

    @NotNull
    @Autowired
    private final ITaskDtoService taskService;

    @NotNull
    @Autowired
    private final ISessionDtoService sessionService;

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    public UserDto findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull UserDto user = Optional
                .ofNullable(repository.findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        repository.update(user);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final UserDto model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        @NotNull final String userId = model.getId();
        if (!existsById(userId)) throw new UserNotFoundException();
        taskService.removeAll(userId);
        projectService.removeAll(userId);
        sessionService.removeAll(userId);
        repository.removeOne(model);
    }

    @Override
    @Transactional
    public void removeOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        removeOne(
                Optional.ofNullable(findByLogin(login))
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @Override
    @Transactional
    public void removeOneByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        removeOne(
                Optional.ofNullable(findByEmail(email))
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @NotNull
    @Override
    @Transactional
    public UserDto setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDto user = Optional
                .ofNullable(repository.findOneById(id))
                .orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final String fName = (firstName == null) ? "" : firstName;
        @NotNull final String lName = (lastName == null) ? "" : lastName;
        @NotNull final String mName = (middleName == null) ? "" : middleName;
        @NotNull final UserDto user = Optional
                .ofNullable(repository.findOneById(id))
                .orElseThrow(UserNotFoundException::new);
        user.setFirstName(fName);
        user.setLastName(lName);
        user.setMiddleName(mName);
        repository.update(user);
        return user;
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull UserDto user = Optional
                .ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        repository.update(user);
    }

}
