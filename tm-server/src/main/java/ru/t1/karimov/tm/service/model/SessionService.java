package ru.t1.karimov.tm.service.model;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.service.model.ISessionService;
import ru.t1.karimov.tm.model.Session;

@Service
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

}
