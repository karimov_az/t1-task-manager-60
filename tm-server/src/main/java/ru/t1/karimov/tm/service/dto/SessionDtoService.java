package ru.t1.karimov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.service.dto.ISessionDtoService;
import ru.t1.karimov.tm.dto.model.SessionDto;

@Service
@AllArgsConstructor
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDto> implements ISessionDtoService {

}
