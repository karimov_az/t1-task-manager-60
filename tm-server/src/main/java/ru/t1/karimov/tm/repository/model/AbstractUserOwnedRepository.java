package ru.t1.karimov.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.karimov.tm.model.AbstractUserOwnedModel;
import ru.t1.karimov.tm.model.User;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M>implements IUserOwnedRepository<M> {

    @NotNull
    protected static final String USER_ID = "userId";

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUser(entityManager.find(User.class, userId));
        add(model);
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final List<M> records = findAll(userId);
        for (@NotNull final M model : records) {
            entityManager.remove(model);
        }
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final String userId, @NotNull final M model) throws Exception {
        @Nullable final M removedModel = findOneById(userId, model.getId());
        entityManager.remove(removedModel);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        removeOne(userId, model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        removeOne(userId, model);
        return model;
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUser(entityManager.find(User.class, userId));
        update(model);
    }

}
