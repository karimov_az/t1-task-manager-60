package ru.t1.karimov.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.IListener;
import ru.t1.karimov.tm.listener.AbstractListener;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class FileScanner {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @Nullable
    @Autowired
    private AbstractListener[] abstractListeners;

    @Nullable
    private final File folder = new File("./");

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private boolean checkCommandName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return false;
        for (@Nullable final IListener listener : abstractListeners) {
            if (listener == null) return false;
            if (name.equals(listener.getName())) return true;
        }
        return false;
    }

    public void init() {
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (@NotNull final File file: folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = checkCommandName(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (@NotNull Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void start() {
        init();
    }

    public void stop () {
        es.shutdown();
    }

}
