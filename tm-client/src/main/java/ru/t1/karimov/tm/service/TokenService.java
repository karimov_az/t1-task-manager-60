package ru.t1.karimov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.service.ITokenService;

@Getter
@Setter
@Service
@NoArgsConstructor
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}
