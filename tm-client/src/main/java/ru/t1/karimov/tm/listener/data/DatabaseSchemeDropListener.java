package ru.t1.karimov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.domain.DatabaseSchemeDropRequest;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.event.ConsoleEvent;

@Component
public final class DatabaseSchemeDropListener extends AbstractDataListener {

    @NotNull
    public static final String DESCRIPTION = "Drop database scheme.";

    @NotNull
    public static final String NAME = "database-drop-scheme";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@databaseSchemeDropListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        domainEndpoint.dropDatabaseScheme(new DatabaseSchemeDropRequest(getToken()));
    }

}
